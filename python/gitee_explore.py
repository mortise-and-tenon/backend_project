import requests
from bs4 import BeautifulSoup

import mongodb_utils

headers = {
    # 在浏览器中，network查看
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
}


if __name__ == '__main__':
    url = "https://gitee.com/explore?utm_source=gitee_search"
    resp = requests.get(url)
    soup = BeautifulSoup(resp.text, 'html.parser')

    items = soup.find_all(class_="explore-categories__item")
    threads = []
    for item in items:
        # print(item)
        title = item.find("a").get_text().replace("\n", "")
        sub_items = item.find_all('a')
        # r = range(0, subItems.length)
        for index in range(len(sub_items)):
            if 0 == index:
                continue
            item = sub_items[index]
            sub_title = item.get_text()
            soup = BeautifulSoup(requests.get("https://gitee.com" + item['href']).text, 'html.parser')
            total = 0
            if None == soup.find(class_="column center aligned"):
                total = len(soup.find(class_="explore-projects__detail-list").find_all(class_="stars-count"))
            else:
                pages = soup.find(class_="column center aligned").find_all(class_="item")
                page_count = int(pages[len(pages) - 2].get_text())
                page_bs = BeautifulSoup(requests.get(
                    "https://gitee.com" + item['href'] + "?page=" + pages[len(pages) - 2].get_text()).text,
                                        'html.parser')
                last_page_count = len(page_bs.find(class_="explore-projects__detail-list").find_all(class_="stars-count"))
                total = (page_count - 1) * 15 + last_page_count

            explore_data = {
                "title": title,
                "sub_title": sub_title,
                "total": total,
                "url": "https://gitee.com" + sub_items[index]['href']
            }

            print(explore_data)

            explore = mongodb_utils.explore.find_one({"title": sub_title})
            if None != explore:
                mongodb_utils.explore.update_one({explore, {"$set": explore_data}})
            else:
                mongodb_utils.explore.insert_one(explore_data)