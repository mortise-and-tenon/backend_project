# coding:utf-8
import json
from os import path
import chnSegment
import plotWordcloud


if __name__=='__main__':

    with open("userdict/backend.json", encoding='utf-8') as f:
        projects = json.load(f)
        with open("doc/backend.txt", "wb") as b:
            for project in projects:
                desc = project['projectDesc']
                desc = desc.replace('的', ' ')
                desc = desc.replace('与', ' ')
                desc = desc.replace('和', ' ')
                desc = desc.replace('了', ' ')
                desc = desc.replace('是', ' ')
                desc = desc.replace('等', ' ')
                desc = desc.replace('以及', ' ')
                desc = desc.replace('使用', ' ')
                desc = desc.replace('项目', ' ')
                desc = desc.replace('模块', ' ')
                desc = desc.replace('集成', ' ')
                desc = desc.replace('系统', ' ')
                desc = desc.replace('开发', ' ')
                desc = desc.replace('基于', ' ')
                desc = desc.replace('一个', ' ')
                desc = desc.replace('实现', ' ')
                desc = desc.replace('采用', ' ')
                desc = desc.replace('可', ' ')
                desc = desc.replace('Spring Boot', 'SpringBoot')
                b.write(bytes(desc, encoding='utf-8'))
                b.write(bytes("\n", encoding='utf-8'))

    # 读取文件
    d = path.dirname(__file__)
    # text = open(path.join(d, 'doc//十九大报告全文.txt')).read()
    # text = open(path.join(d, 'doc//2017年中央政府工作报告(全文).txt')).read()
    # text = open(path.join(d,'doc//alice.txt')).read()
    text = open(path.join(d, 'doc/backend.txt')).read()
    #  text="付求爱很帅并来到付求爱了网易研行大厦很帅 很帅 很帅"

    # 若是中文文本，则先进行分词操作
    text= chnSegment.word_segment(text)
    
    # 生成词云
    plotWordcloud.generate_wordcloud(text)
