import json

if __name__ == '__main__':
    with open("userdict/backend.json", encoding='utf-8') as f:
        projects = json.load(f)
        with open("doc/backend.txt", "wb") as b:
            for project in projects:
                desc = project['projectDesc']
                desc = desc.replace('的', ' ')
                desc = desc.replace('和', ' ')
                desc = desc.replace('使用', ' ')
                desc = desc.replace('项目', ' ')
                b.write(bytes(desc, encoding='utf-8'))
                b.write(bytes("\n", encoding='utf-8'))
