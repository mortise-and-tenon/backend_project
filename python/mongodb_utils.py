import pymongo

# mongodb 地址
client = pymongo.MongoClient("mongodb://localhost:27017")
# 数据库名称
db_name = 'gitee'
# 客户端数据库
giteeClient = client[db_name]
# 后台管理系统
backendClient = giteeClient["backend"]
explore = giteeClient["explore"]
