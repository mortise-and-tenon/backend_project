import mongodb_utils

def getLanguageDetail(l):
    if "Java" == l:
        return "Java", 910
    if "C#" == l:
        return l, 561
    if "Go" == l:
        return l, 237
    if "PHP" == l:
        return l, 371
    return "", 0

def genMd(l):
    language, star = getLanguageDetail(l)

    projects = mongodb_utils.backendClient.find(
        {"$and": [
            {"$or": [{"license": "MIT"}, {"license": "Apache-2.0"}]},
            {"homepage": {"$ne": ""}},
            {"language": language},
            {"star": {"$gt": star}},
            {"lastUpdateTime": {"$gt": "2022-04-01 00:00:00"}}
        ]}
    ).sort("star", -1)

    print("### " + language)
    for project in projects:
        firstLine = "项目名称：" + project.get('projectName')
        if project.get('gvp'):
            firstLine += "，**GVP项目**"
        if project.get('recommended'):
            firstLine += "，**推荐项目**"
        print(firstLine)
        print("[项目地址](" + project.get('url') + ")" + "，star数量：" + str(
            project.get('star')))
        tags = project['tags']
        if len(tags) > 0:
            tagStr = "标签"
            for i in range(len(tags)):
                tags[i] = '`' + tags[i] + '` '
            print("标签：" + ",".join(tags))
        print("[官网地址](" + project.get('homepage') + ")")
        print("项目特色：" + project.get('projectDesc') + "")
        print()
        print("---")
        print()

def genStudy(l, star, condition):
    projects = mongodb_utils.backendClient.find(
        {"$and":[
        {"homepage": {"$ne": ""}},
        {"language": l},
        {"star": {"$gt": star}},
        {"lastUpdateTime": {"$gt": "2022-04-01 00:00:00"}},
        {"$or":condition}
    ]}
    )
    print("### " + l)
    for project in projects:
        firstLine = "项目名称：" + project.get('projectName')
        if project.get('gvp'):
            firstLine += "，**GVP项目**"
        if project.get('recommended'):
            firstLine += "，**推荐项目**"
        print(firstLine)
        print("[项目地址](" + project.get('url') + ")" + "，star数量：" + str(
            project.get('star')))
        tags = project['tags']
        if len(tags) > 0:
            tagStr = "标签"
            for i in range(len(tags)):
                tags[i] = '`' + tags[i] + '` '
            print("标签：" + ",".join(tags))
        print("[官网地址](" + project.get('homepage') + ")")
        print("项目特色：" + project.get('projectDesc') + "")
        print()
        print("---")
        print()

if __name__ == '__main__':
    # genMd("Java")
    # genMd("C#")
    # genMd("Go")
    # genMd("PHP")
    # genStudy("Java", 2000, [{"projectDesc": {'$regex':'Vue3'}}, {"projectDesc": {'$regex':'SpringBoot'}}, {"projectDesc": {'$regex':'分布式'}}, {"projectDesc": {'$regex':'Cloud'}}])
    genStudy("JavaScript", 2000, [{"projectDesc": {'$regex':'Vue'}}, {"projectDesc": {'$regex':'ant'}}])
    genStudy("TypeScript", 2000, [{"projectDesc": {'$regex':'Vue'}}, {"projectDesc": {'$regex':'ant'}}])
    genStudy("C#", 500, [{"projectDesc": {'$regex':''}}])
    genStudy("Go", 500, [{"projectDesc": {'$regex':''}}])
