import json
import threading

import requests
from bs4 import BeautifulSoup

from mongodb_utils import backendClient

headers = {
    # 在浏览器中，network查看
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
}

def gitScore(url, token):
    score = 0
    headers = {
        'Accept':'application/json, text/javascript, */*; q=0.01',
        'Accept-Language':'zh,zh-CN;q=0.9,en;q=0.8',
        'Connection':'keep-alive',
        'Referer':url,
        'Sec-Fetch-Dest':'empty',
        'Sec-Fetch-Mode':'cors',
        'Sec-Fetch-Site':'same-origin',
        'User-Agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36',
        'X-CSRF-Token':token,
        'X-Requested-With':'XMLHttpRequest',
        'sec-ch-ua':'"Chromium";v="106", "Google Chrome";v="106", "Not;A=Brand";v="99"',
        'sec-ch-ua-mobile':'?0',
        'sec-ch-ua-platform':'"macOs'
    }

    resp = requests.get(url+"/project_radars", headers=headers)
    score = json.loads(resp.text)
    return score

def loadProject(url):
    session = requests.Session()
    resp = session.get(url, headers=headers)
    # csrf = re.search(r'name="_csrf".+value="([^"]+)"', resp.text).group(1)

    soup = BeautifulSoup(resp.text, 'html.parser')
    # print(soup.prettify())

    contents = soup.find_all(class_='content')

    for content in contents:
        url = "https://gitee.com" + content.find(class_="project-title").h3.a['href']

        detailResp = session.get(url, headers=headers)
        detail = BeautifulSoup(detailResp.text, 'html.parser')
        # print(content)
        # 项目名
        project_name = content.find(class_="project-title").h3.a.get_text()

        lastUpdateTime = content.find(class_="text-muted project-item-bottom__item d-flex-center")['title']

        # 是否推荐项目
        author = project_name.split('/')[0]

        # 是否推荐项目
        name = project_name.split('/')[1]

        # 是否GVP
        gvp = None != content.find(title="GVP - Gitee 最有价值开源项目")

        # 是否推荐项目
        recommended = None != content.find(class_="iconfont icon-recommended js-popup-default")

        # star数量
        starCount = content.find(class_="stars-count")['data-count']

        # 项目简介
        projectDesc = content.find(class_="project-desc mb-1")['title']

        # 标签
        tags = []
        tagList = content.find_all(class_="project-label-item")
        for tag in tagList:
            tags.append(tag.get_text())

        # 编程语言
        languageTag = content.find(class_="project-language project-item-bottom__item")
        language = ""
        if languageTag != None:
            language = languageTag.get_text()
        else:
            if None != detail.find(class_='percentage'):
                if None != detail.find(href=detail.find(class_='percentage')['href']):
                    language = detail.find(href=detail.find(class_='percentage')['href']).get_text()

        readMeUrl = url + "/blob/master/README.md"

        # readMeContent = requests.get(url + "/raw/master/README.md").text
        license = ""
        if None != detail.find(id="license-popup"):
            license = detail.find(id="license-popup").get_text()

        homepage = ""
        if None != detail.find(id="homepage"):
            homepage = detail.find(id="homepage").get_text()

        token = soup.find(attrs={"name": "csrf-token"})['content']
        score = gitScore(url, token)

        # url:
        jsonData = {
            # 作者
            "author": author,
            # 项目名称
            "name": name,
            # 项目名称 若依/RuoYi
            "projectName": project_name,
            # 项目地址
            "url": url,
            # 是否为GVP项目
            "gvp": gvp,
            # 是否为推荐项目
            "recommended": recommended,

            "star": int(starCount),
            "projectDesc": projectDesc,
            "tags": tags,
            "language": language,
            "lastUpdateTime": lastUpdateTime,
            "readMe": readMeUrl,
            "homepage": homepage,
            "license": license,
            "giteeScore": score
        }

        print(jsonData)
        project = backendClient.find_one({"projectName":project_name})
        if None!= project:
            backendClient.update_one(project, {"$set":jsonData})
        else:
            backendClient.insert_one(jsonData)



if __name__ == '__main__':
    r = range(1, 70)
    threads = []
    for i in r:
        url = "https://gitee.com/explore/backend"
        if i > 1:
            url += '?page=' + i.__str__()
        thread = threading.Thread(target=loadProject, args=(url,))
        threads.append(thread)
        thread.start()

    for t in threads:
        t.join()
